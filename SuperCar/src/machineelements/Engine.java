
package machineelements;

public class Engine {
    
    private int cylinders = 6;
    private int pistons   = 6;
    private String name = "Двигатель";

    public Engine(int cylinders,int pistons){
    
    this.cylinders = cylinders;
    this.pistons = pistons;
    System.out.println("Двигатель внутреннего сгорания состоит из"+cylinders+"и"+pistons);
    
    }
     
    public int getCylinders() {
        return cylinders;
    }

    public int getPistons() {
        return pistons;
    }

    public String getName() {
        return name;
    }
}
