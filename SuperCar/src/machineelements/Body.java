/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package machineelements;

   public class Body {
    
   private int spoiler;
   private int doors = 4;
   private int headlights;
   private int windshield;
   private String name = "Кузов";
   
   public Body(int spoiler, int doors, int headlights, int windshield){
       
       this.doors = doors;
       this.headlights = headlights;
       this.spoiler = spoiler;
       this.windshield = windshield;
       
       System.out.println("Кузов состоит из:"+spoiler+doors+headlights+windshield);
   }
   
    public int getSpoiler() {
        return spoiler;
    }

    public int getDoors() {
        return doors;
    }

    public int getHeadlights() {
        return headlights;
    }

    public int getWindshield() {
        return windshield;
    }

    public String getName() {
        return name;
    }
}
