
package machineelements;

import machineelements.Body;
import machineelements.Engine;
import machineelements.Transmission;

public class SuperCar {

    int cylinders;
    int pistons;
    int gears;
    int spoiler;
    int doors;
    int headlights;
    int windshield;
    
    Body body;
    Transmission transmission;
    Engine engines;
 
    public SuperCar(int cylinders, int pistons, int gears, int spoiler, int doors, int headlights, int windshield){
       this.cylinders = cylinders;
       this.pistons = pistons;
       this.doors = doors;
       this.headlights = headlights;
       this.spoiler = spoiler;
       this.windshield = windshield;
       this.gears = gears;
    }
  
    
    public  SuperCar createNewSuperCar(int cylinders, int pistons, int gears, int spoiler, int doors, int headlights, int windshield){
        this.engines = new Engine(cylinders,pistons);
        this.transmission = new Transmission(gears);
        this.body = new Body(spoiler,doors,headlights,windshield);
        
        SuperCar superCar = new SuperCar(cylinders,pistons,gears, spoiler,doors, headlights,windshield);
        
        return superCar;
    }
}
