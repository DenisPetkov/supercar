
package machineelements;

public class Transmission {
    
    private int gears = 6;
    private String name = "Коробка передач";
    
    public Transmission(int gears){
       
       this.gears = gears;
       System.out.println("Коробка передач состоит из"+gears);
    }
    
 public class Gears{
 
    private int numberTeeth = 3;// Количество зубьев на шестеренке
    private int numberGears = 3;// Количество шестеренок
    String name = "Шестеренки";
   
    public int getNumberTeeth() {
        return numberTeeth;
    }

    public int getNumberGears() {
        return numberGears;
    }
  }   
}
